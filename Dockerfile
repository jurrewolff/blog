FROM golang:1.16 AS build

WORKDIR /src
COPY . .

RUN go build -o /out/main cmd/blog/main.go


FROM debian:latest
WORKDIR /app

COPY --from=build /out/main .
COPY configs configs
COPY examples examples
COPY pkg pkg
COPY web web

RUN mkdir db && ./main -migrate 1
ENTRYPOINT ["./main"]
