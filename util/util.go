package util

import (
	"fmt"
	"strings"
)

func GetFileExtension(fileName string) (string, error) {
	i := strings.Index(fileName, ".")
	if i == -1 {
		return fileName, fmt.Errorf("no file extension present")
	}

	extChars := len(fileName) - i
	return fileName[len(fileName)-extChars:], nil
}

func StrSliceContains(slice []string, target string) bool {
	for _, str := range slice {
		if str == target {
			return true
		}
	}
	return false
}
