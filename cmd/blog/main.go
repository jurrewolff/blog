package main

import (
	"blog/pkg/config"
	"blog/pkg/database"
	"blog/pkg/errors"
	"blog/pkg/flag"
	"blog/pkg/logging"
	"blog/pkg/post"
	"blog/pkg/route"
	"fmt"
	"net/http"
)

func main() {
	c := config.Config{}
	err := c.Load("configs/config.yaml", true)
	if err != nil {
		msg := "error parsing config"
		errors.HandleError(msg, err)
	}

	// TODO - Logging package should parse log configuration.
	logger := logging.New()

	db, err := database.InitDB(c)
	if err != nil {
		msg := "error initializing db"
		errors.HandleError(msg, err)
	}

	err = flag.ProcessFlags(db)
	if err != nil {
		msg := "error processing flags"
		errors.HandleError(msg, err)
	}

	err = post.ParsePosts(c.Posts.Path, db)
	if err != nil {
		msg := "error parsing posts"
		errors.HandleError(msg, err)
	}

	err = post.CleanDeletedPosts(c, db)
	if err != nil {
		msg := "error cleaning deleted posts"
		errors.HandleError(msg, err)
	}

	router := route.SetRoutes(db, logger)

	err = serve(c.Webserver.Address, c.Webserver.Port, router, logger)
	if err != nil {
		msg := "error starting server"
		errors.HandleError(msg, err)
	}
}

func serve(address string, port int, r *http.ServeMux, logger *logging.Logger) error {
	serverAddress := fmt.Sprintf("%s:%d", address, port)

	err := logger.Println(logging.INFO, fmt.Sprintf("Listening on %s", serverAddress))
	if err != nil {
		return fmt.Errorf("error printing log message: %w", err)
	}

	err = http.ListenAndServe(serverAddress, r)
	if err != nil {
		return fmt.Errorf("error starting webserver: %w", err)
	}

	return nil
}
