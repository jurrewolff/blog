# blog

Lightweight blog for myself.

# Goals

* Simple and lightweight HTML/CSS only blog application.
* Docker image for easy deployment/management.

# Usage

A few steps are required on initial setup:

1. Migrate database
    - `go run cmd/blog/main.go -migrate 1`
2. Start application
    - development: `go run cmd/blog/main.go`
    - production: `go build cmd/blog/main.go && ./main`

## Production db migrations

When updating the database models in a production environment, new migrations can be generated (see details here).

Migrations can be performed using the '-migrate' flag, accompanied by an integer 'n' value, either positive for
migrating 'n' steps upwards or negative for undoing migrations with 'n' steps.

> With the 'migrate' flag present, the application will exit after migrating.    
