# Example markdown blogpost 

Date: 11-07-2022

Author: Jurre Wolff

## Explanation
This file serves as an example of the Markdown blogpost format.

The first header represents the blogpost title. Below the title, the "date" and "author" fields are required, including
the blank line in between.

Afterwards any regular Markdown text can follow!

See [this link](https://www.markdownguide.org/cheat-sheet/) for cool Markdown formatting thingies.

## Various markdown features

> Note: You can make notes!

---

**Bullets:**
- Bullet 1
  1. Step unos
  2. Step dos
- Bullet 2
- Bullet 3

_Italic text is nice for highlighting things_

```go
txt := "Does it do code snippets???"
func(txt)

func Demo(txt string) error {
	err := fmt.PrintLn(txt)
	return err
}
```
