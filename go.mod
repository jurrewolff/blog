module blog

go 1.18

require (
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/jmoiron/sqlx v1.3.5
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mattn/go-sqlite3 v1.14.14
	github.com/yuin/goldmark v1.4.13
	gopkg.in/yaml.v2 v2.4.0
)
