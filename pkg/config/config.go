package config

import (
	"fmt"
	"os"

	"github.com/kelseyhightower/envconfig"
	"gopkg.in/yaml.v2"
)

type Config struct {
	Webserver struct {
		Address string `yaml:"address" ,envconfig:"WEBSERVER_ADDRESS"`
		Port    int    `yaml:"port" ,envconfig:"WEBSERVER_PORT"`
	}
	Posts struct {
		Path string `yaml:"path" ,envconfig:"POSTS_PATH"`
	}
	Database struct {
		Type     string `yaml:"type" ,envconfig:"DATABASE_TYPE"`
		File     string `yaml:"file" ,envconfig:"DATABASE_FILE"`
		Name     string `yaml:"name" ,envconfig:"DATABASE_NAME"`
		Host     string `yaml:"host" ,envconfig:"DATABASE_HOST"`
		Port     int    `yaml:"port" ,envconfig:"DATABASE_PORT"`
		User     string `yaml:"user" ,envconfig:"DATABASE_USER"`
		Password string `yaml:"password" ,envconfig:"DATABASE_PASSWORD"`
	}
	Log struct { // TODO - Dynamic handler parsing
		Handlers struct {
			Console struct {
				Format string `yaml:"format"`
				Level  int    `yaml:"level"`
			} `yaml:"console"`
			File struct {
				Format string `yaml:"format"`
				Level  int    `yaml:"level"`
				Out    string `yaml:"out"`
			} `yaml:"file"`
			Dev struct {
				Format string `yaml:"format"`
				Level  int    `yaml:"level"`
			} `yaml:"dev"`
		} `yaml:"handlers"`
		Handler []string `yaml:"handler"`
	} `yaml:"log"`
}

func (c *Config) Load(file string, loadEnv bool) error {
	var err error

	if file != "" {
		err = c.loadCfgFile(file)
		if err != nil {
			return fmt.Errorf("error reading config file: %w", err)
		}
	}

	if loadEnv {
		err = c.loadCfgEnv()
		if err != nil {
			return fmt.Errorf("error reading environment config: %w", err)
		}
	}

	return nil
}

func (c *Config) loadCfgFile(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("error opening config file %s: %w", file, err)
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&c)
	if err != nil {
		return fmt.Errorf("error decoding config file %s: %w", file, err)
	}
	return nil
}

func (c *Config) loadCfgEnv() error {
	err := envconfig.Process("", c)
	if err != nil {
		return fmt.Errorf("error processing environment variables into config: %w", err)
	}
	return nil
}
