package config

import (
	"os"
	"testing"
)

const testConfig = "../../test/data/pkg/config/config_test.yaml"

var expectedConfig = Config{
	Webserver: struct {
		Address string `yaml:"address" ,envconfig:"WEBSERVER_ADDRESS"`
		Port    int    `yaml:"port" ,envconfig:"WEBSERVER_PORT"`
	}{
		Address: "127.0.0.1",
		Port:    3000,
	},
	Posts: struct {
		Path string `yaml:"path" ,envconfig:"POSTS_PATH"`
	}{Path: "./examples/posts/"},
	Database: struct {
		Type     string `yaml:"type" ,envconfig:"DATABASE_TYPE"`
		File     string `yaml:"file" ,envconfig:"DATABASE_FILE"`
		Name     string `yaml:"name" ,envconfig:"DATABASE_NAME"`
		Host     string `yaml:"host" ,envconfig:"DATABASE_HOST"`
		Port     int    `yaml:"port" ,envconfig:"DATABASE_PORT"`
		User     string `yaml:"user" ,envconfig:"DATABASE_USER"`
		Password string `yaml:"password" ,envconfig:"DATABASE_PASSWORD"`
	}{
		Type:     "postgresql",
		File:     "",
		Name:     "dbName",
		Host:     "dbHost",
		Port:     5432,
		User:     "dbUser",
		Password: "dbPassword",
	},
}

//mockFuncs := []struct {
//	name            string
//	userID          int
//	mockFunc        func()
//	expectedProfile UserProfile
//	expectingErr    bool
//}{
//	{
//		name:   "All success no error",
//		userID: 10,
//		mockFunc: func() {
//			GetPersonByID = func(id int) (*Person, error) {
//				return &Person{
//					ID:       10,
//					BornDate: "1989-12-12",
//					Name:     "Jack",
//				}, nil
//			}
//
//			GetPersonAddrByUserID = func(userID int) (*Address, error) {
//				return &Address{
//					ID:         123,
//					UserID:     10,
//					PostalCode: "12320",
//					Street:     "Jl. XYZ, No 24, ABC, EFG",
//				}, nil
//			}
//		},
//		expectedProfile: UserProfile{
//			User: Person{
//				ID:       10,
//				BornDate: "1989-12-12",
//				Name:     "Jack",
//			},
//			Address: Address{
//				ID:         123,
//				UserID:     10,
//				PostalCode: "12320",
//				Street:     "Jl. XYZ, No 24, ABC, EFG",
//			},
//		},
//		expectingErr: false,
//	},
//	{
//		name:   "Error in get person by id",
//		userID: 10,
//		mockFunc: func() {
//			GetPersonByID = func(id int) (*Person, error) {
//				return nil, sql.ErrConnDone
//			}
//
//			GetPersonAddrByUserID = func(userID int) (*Address, error) {
//				return &Address{
//					ID:         123,
//					UserID:     10,
//					PostalCode: "12320",
//					Street:     "Jl. XYZ, No 24, ABC, EFG",
//				}, nil
//			}
//		},
//		expectedProfile: UserProfile{},
//		expectingErr:    true,
//	},
//	{
//		name:   "Error in get person address by user id",
//		userID: 10,
//		mockFunc: func() {
//			GetPersonByID = func(id int) (*Person, error) {
//				return &Person{
//					ID:       10,
//					BornDate: "1989-12-12",
//					Name:     "Jack",
//				}, nil
//			}
//
//			GetPersonAddrByUserID = func(userID int) (*Address, error) {
//				return nil, sql.ErrConnDone
//			}
//		},
//		expectedProfile: UserProfile{},
//		expectingErr:    true,
//	},
//}

func TestLoadFileAndEnv(t *testing.T) {
	modifiedExpectedConfig := expectedConfig
	modifiedExpectedConfig.Webserver.Address = "10.0.0.1"
	modifiedExpectedConfig.Webserver.Port = 1111

	err := os.Setenv("WEBSERVER_ADDRESS", "10.0.0.1")
	if err != nil {
		t.Fatalf("error setting environment variable: %s", err)
	}

	err = os.Setenv("WEBSERVER_PORT", "1111")
	if err != nil {
		t.Fatalf("error setting environment variable: %s", err)
	}

	var c Config
	err = c.Load(testConfig, true)
	if err != nil {
		t.Fatalf("loading valid config from file and environment failed with error: %s", err)
	}

	if c != modifiedExpectedConfig {
		t.Errorf("loaded config != expected config")
	}
}

func TestLoadFile(t *testing.T) {
	err := os.Setenv("WEBSERVER_ADDRESS", "10.0.0.1")
	if err != nil {
		t.Fatalf("error setting environment variable: %s", err)
	}

	err = os.Setenv("WEBSERVER_PORT", "1111")
	if err != nil {
		t.Fatalf("error setting environment variable: %s", err)
	}

	var c Config
	err = c.Load(testConfig, false)
	if err != nil {
		t.Fatalf("loading valid config from file and environment failed with error: %s", err)
	}

	if c != expectedConfig {
		t.Errorf("loaded config != expected config")
	}
}

func TestLoadEnv(t *testing.T) {
	var modifiedExpectedConfig Config
	modifiedExpectedConfig.Webserver.Address = "10.0.0.1"
	modifiedExpectedConfig.Webserver.Port = 1111

	err := os.Setenv("WEBSERVER_ADDRESS", "10.0.0.1")
	if err != nil {
		t.Fatalf("error setting environment variable: %s", err)
	}

	err = os.Setenv("WEBSERVER_PORT", "1111")
	if err != nil {
		t.Fatalf("error setting environment variable: %s", err)
	}

	var c Config
	err = c.Load("", true)
	if err != nil {
		t.Fatalf("loading valid config from file and environment failed with error: %s", err)
	}

	if c != modifiedExpectedConfig {
		t.Errorf("loaded config != expected config")
	}
}

func TestLoadReadFileError(t *testing.T) {
	// TODO - Preparation setting Load() up to encounter error from loadCfgFile() method.

	var c Config
	err := c.Load(testConfig, false)
	if err == nil {
		t.Fatalf("loading of config continued after loadCfgFile() method errored.")
	}
}
