package flag

import (
	"blog/pkg/database"
	"flag"
	"fmt"
	"os"
)

var (
	pMigrateSteps = flag.Int("migrate", 0, "Migrate \"n\" steps up or down (Accepts both positive and negative values)")
)

func ProcessFlags(db database.PostDB) error {
	flag.Parse()

	if *pMigrateSteps != 0 {
		err := db.Migrate(*pMigrateSteps)
		if err != nil {
			return fmt.Errorf("error migrating db: %w", err)
		}
		os.Exit(0)
	}

	return nil
}
