-- Timestamp format (The Go interpretation of RFC3339): 2021-07-14T13:59:07+02:00

CREATE TABLE IF NOT EXISTS "post"
(
    "post_id"       INTEGER          UNIQUE,
    "filepath"      TEXT    NOT NULL UNIQUE,
    "filename"      TEXT    NOT NULL UNIQUE,
    "gen_template"  TEXT             UNIQUE,
    "gen_post"      TEXT    NOT NULL UNIQUE,
    "modtime"       TEXT    NOT NULL,
    "title"         TEXT    NOT NULL,
    "timestamp"     TEXT    NOT NULL DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ+00:00', 'now')),
    "author"        TEXT    NOT NULL,
    "likes"         INTEGER NOT NULL DEFAULT 0,
    "dislikes"      INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY ("post_id" AUTOINCREMENT)
);
