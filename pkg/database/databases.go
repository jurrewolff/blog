package database

import (
	"blog/pkg/config"
	"fmt"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/sqlite3"
	"reflect"
	"time"

	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type Post struct {
	PostID      int     `db:"post_id"` // Auto-incremented when '0' while inserting.
	FilePath    string  `db:"filepath"`
	FileName    string  `db:"filename"`
	GenTemplate *string `db:"gen_template"`
	GenPost     string  `db:"gen_post"`
	ModTime     string  `db:"modtime"`
	Title       string  `db:"title"`
	Timestamp   string  `db:"timestamp"`
	Author      string  `db:"author"`
	//Tags      []string  // TODO support 'tags' in DB
	Likes    int `db:"likes"`
	Dislikes int `db:"dislikes"`
	//Comments  []Comment // TODO support 'comments' in DB
}

type Comment struct {
	Author    string
	Timestamp time.Time
	Text      string
}

type PostDB interface {
	connect(c config.Config) error
	Migrate(steps int) error
	InsertPost(p Post) error
	SelectPost(p Post) ([]Post, error)
	UpdatePost(p Post) error
	DeletePost(p Post) error
}

type SQLiteDB struct {
	db *sqlx.DB
}

func (S *SQLiteDB) connect(c config.Config) error {
	var err error
	var dsn = fmt.Sprintf("file:%s", c.Database.File)

	S.db, err = sqlx.Connect("sqlite3", dsn)
	if err != nil {
		return fmt.Errorf("failed to connect to sqlite db with dsn '%s': %w", dsn, err)
	}

	return nil
}

func (S *SQLiteDB) Migrate(steps int) error {
	d, err := sqlite3.WithInstance(S.db.DB, &sqlite3.Config{})
	if err != nil {
		return fmt.Errorf("error getting db driver instance: %w", err)
	}

	m, err := migrate.NewWithDatabaseInstance(
		"file://./pkg/database/migrations",
		"sqlite3", d)
	if err != nil {
		return fmt.Errorf("error getting migrate instance: %w", err)
	}

	err = m.Steps(steps)
	if err != nil {
		return fmt.Errorf("error migrating '%d' step(s): %w", steps, err)
	}

	return nil
}

func (S *SQLiteDB) InsertPost(p Post) error {
	// TODO - Timestamp format enforcement
	q, err := buildSqlxQueryString(p, "post_id", "insert", "post")
	if err != nil {
		return fmt.Errorf("error building sqlx querystring: %w", err)
	}

	namedStmt, err := S.db.PrepareNamed(q)
	if err != nil {
		return fmt.Errorf("error preparing named statement: %s", err)
	}

	_, err = namedStmt.Exec(p)
	if err != nil {
		return fmt.Errorf("error inserting row into table 'post': %s", err) // TODO - Create custom db errs?
	}

	return nil
}

// SelectPost queries db for posts matching all provided struct fields of p.
func (S *SQLiteDB) SelectPost(p Post) ([]Post, error) {
	var posts []Post

	q, err := buildSqlxQueryString(p, "post_id", "select", "post")
	if err != nil {
		return posts, fmt.Errorf("error building sqlx querystring: %w", err)
	}

	rows, err := S.db.NamedQuery(q, p)
	if err != nil {
		return posts, fmt.Errorf("error preparing named statement: %w", err)
	}
	defer func() {
		if err = rows.Close(); err != nil {
			panic("error closing db rows")
		}
	}()

	for rows.Next() {
		var pRes Post
		err = rows.StructScan(&pRes)
		if err != nil {
			return posts, fmt.Errorf("error scanning row into struct: %w", err)
		}

		posts = append(posts, pRes)
	}

	return posts, nil
}

func (S *SQLiteDB) UpdatePost(p Post) error {
	q, err := buildSqlxQueryString(p, "post_id", "update", "post")
	if err != nil {
		return fmt.Errorf("error building sqlx querystring: %w", err)
	}

	_, err = S.db.NamedExec(q, p)
	if err != nil {
		return fmt.Errorf("error updating row from table 'post': %w", err)
	}

	return nil
}

func (S *SQLiteDB) DeletePost(p Post) error {
	q, err := buildSqlxQueryString(p, "post_id", "delete", "post")
	if err != nil {
		return fmt.Errorf("error building sqlx querystring: %w", err)
	}

	_, err = S.db.NamedExec(q, p)
	if err != nil {
		return fmt.Errorf("error deleting row from table 'post': %w", err)
	}

	return nil
}

func InitDB(c config.Config) (PostDB, error) {
	var err error
	var postDB PostDB

	switch c.Database.Type {
	case "sqlite3":
		postDB = &SQLiteDB{}
	case "":
		return nil, fmt.Errorf("db type not set in config")
	default:
		return nil, fmt.Errorf("db type not implemented")
	}

	err = postDB.connect(c)
	if err != nil {
		return nil, fmt.Errorf("error connecting to db: %w", err)
	}
	return postDB, nil
}

// buildSqlxQueryString processes a struct representing a db, a primary key column name, an action type and a table
// identifier into a single querystring usable with sqlx.
func buildSqlxQueryString(p interface{}, primaryKey, action, table string) (string, error) {
	var qs string
	var queryVals []string
	var pkID int

	v := reflect.ValueOf(p)
	typeOfP := v.Type()
	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).IsZero() {
			continue
		}

		structTag := typeOfP.Field(i).Tag.Get("db")
		if structTag == primaryKey {
			var ok bool
			fieldVal := v.Field(i).Interface()
			pkID, ok = fieldVal.(int)
			if !ok {
				return qs, fmt.Errorf("pk id of field '%s' is not int", structTag)
			}
		}
		queryVals = append(queryVals, typeOfP.Field(i).Tag.Get("db"))
	}

	switch action {
	case "insert":
		var targetVals string
		var replacingVals string
		for i, val := range queryVals {
			if i == len(queryVals)-1 {
				targetVals += val
				replacingVals += fmt.Sprintf(":%s", val)
				break
			}

			targetVals += fmt.Sprintf("%s, ", val)
			replacingVals += fmt.Sprintf(":%s, ", val)
		}

		qs = fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", table, targetVals, replacingVals)
		return qs, nil
	case "select":
		if len(queryVals) == 0 {
			return fmt.Sprintf("SELECT * FROM %s", table), nil
		}

		var filterStr string
		for i, val := range queryVals {
			if i == len(queryVals)-1 {
				filterStr += fmt.Sprintf("%s = :%s", val, val)
				break
			}
			filterStr += fmt.Sprintf("%s = :%s AND ", val, val)
		}

		qs = fmt.Sprintf("SELECT * FROM %s WHERE %s", table, filterStr)
		return qs, nil
	case "update":
		var updateStr string
		for i, val := range queryVals {
			if i == len(queryVals)-1 {
				updateStr += fmt.Sprintf("%s = :%s", val, val)
				break
			}
			updateStr += fmt.Sprintf("%s = :%s, ", val, val)
		}
		qs = fmt.Sprintf("UPDATE %s SET %s WHERE %s = %d", table, updateStr, primaryKey, pkID)
		return qs, nil
	case "delete":
		var filterStr string
		for i, val := range queryVals {
			if i == len(queryVals)-1 {
				filterStr += fmt.Sprintf("%s = :%s", val, val)
				break
			}
			filterStr += fmt.Sprintf("%s = :%s AND ", val, val)
		}

		qs = fmt.Sprintf("DELETE FROM %s WHERE %s", table, filterStr)
		return qs, nil
	default:
		return qs, fmt.Errorf("action '%s' not supported", action)
	}
}
