package post

import (
	"blog/pkg/config"
	"blog/pkg/database"
	"blog/pkg/errors"
	"blog/util"
	"bufio"
	"bytes"
	"fmt"
	"html/template"
	"os"
	"strings"
	"time"

	"github.com/yuin/goldmark"
)

const tBlogpost = "./web/template/blogpost.html"
const tBlogpostMd = "./web/template/blogpost_md.html"
const tNav = "./web/template/gen_templ/navigation.html"
const tMain = "./web/template/main.html"
const genTemplDir = "./web/template/gen_templ"
const genPostPath = "./web/static/gen_posts"

type ProcessPostError struct {
	Msg string
	Err error
}

func (r *ProcessPostError) Error() string {
	return fmt.Sprintf("error processing posts: %s, :%s", r.Msg, r.Err)
}

// Post Interface allows for parsing different types of blogpost formats
type Post interface {
	parseContent() error
	genHtml() error
	// TODO - Remove newPost arg and cleanup txtpost its implementation not to need this arg.
	saveToDB(db database.PostDB, dbPost database.Post, newPost bool) error
}

type txtPost struct {
	fileName string
	filePath string
	genPost  string
	modTime  string
	author   string
	title    string
	date     string
	body     string
}

type mdPost struct {
	fileName    string
	filePath    string
	genPost     string
	genTemplate string
	modTime     string
	author      string
	title       string
	date        string
	body        string
}

func (mp *mdPost) parseContent() error {
	f, err := os.Open(mp.filePath)
	if err != nil {
		msg := "error opening file"
		return &ProcessPostError{msg, err}
	}

	lineNumber := 0
	s := bufio.NewScanner(f)
	s.Split(bufio.ScanLines)

	var body []string
	for s.Scan() {
		line := s.Text()

		if strings.HasPrefix(line, "# ") && lineNumber == 0 {
			mp.title = strings.TrimPrefix(line, "# ")
			lineNumber++
			continue
		} else if strings.HasPrefix(line, "Date: ") && lineNumber == 2 {
			mp.date = strings.TrimPrefix(line, "Date: ")
			lineNumber++
			continue
		} else if strings.HasPrefix(line, "Author: ") && lineNumber == 4 {
			mp.author = strings.TrimPrefix(line, "Author: ")
			lineNumber++
		} else if lineNumber > 3 {
			body = append(body, line)
		}
		lineNumber++
	}

	mp.body = strings.Join(body, "\n")

	err = f.Close()
	if err != nil {
		msg := "error closing file"
		return &ProcessPostError{msg, err}
	}

	return nil
}

func (mp *mdPost) genHtml() error {
	// Create template HTML file
	ext, err := util.GetFileExtension(mp.fileName)
	if err != nil {
		msg := fmt.Sprintf("error getting file extension for filename '%s'", mp.fileName)
		return &ProcessPostError{msg, err}
	}

	outputName := fmt.Sprintf("gen_%s.html", strings.TrimSuffix(mp.fileName, ext))

	genHtmlFile, err := os.Create(fmt.Sprintf("%s/%s", genTemplDir, outputName))
	if err != nil {
		msg := "error creating file for generating html template"
		return &ProcessPostError{msg, err}
	}
	defer func(genHtmlFile *os.File) {
		err := genHtmlFile.Close()
		if err != nil {
			msg := fmt.Sprintf("error closing file '%s' for generating html template", genHtmlFile.Name())
			errors.HandleError(msg, err)
		}
	}(genHtmlFile)

	// Convert markdown to HTML and write into the template file
	var buf bytes.Buffer
	if err := goldmark.Convert([]byte(mp.body), &buf); err != nil {
		msg := fmt.Sprintf("error converting markdown body of post '%s' to html", mp.fileName)
		return &ProcessPostError{msg, err}
	}

	_, err = genHtmlFile.WriteString("{{define \"md_post\"}}\n")
	if err != nil {
		msg := fmt.Sprintf("error writing to file '%s'", genHtmlFile.Name())
		return &ProcessPostError{msg, err}
	}

	_, err = genHtmlFile.Write(buf.Bytes())
	if err != nil {
		msg := fmt.Sprintf("error writing generated html to file '%s'", genHtmlFile.Name())
		return &ProcessPostError{msg, err}
	}

	_, err = genHtmlFile.WriteString("{{end}}\n")
	if err != nil {
		msg := fmt.Sprintf("error writing to file '%s'", genHtmlFile.Name())
		return &ProcessPostError{msg, err}
	}

	// Generate static page incorporating all page components
	mp.genTemplate = fmt.Sprintf("%s/%s", genTemplDir, outputName)
	t, err := template.ParseFiles(tBlogpostMd, mp.genTemplate, tMain, tNav)
	if err != nil {
		msg := "error parsing template files"
		return &ProcessPostError{msg, err}
	}

	fileName := fmt.Sprintf("%s_md.html", strings.TrimSuffix(mp.fileName, ".md"))
	mp.genPost = fmt.Sprintf("%s/%s", genPostPath, fileName)

	f, err := os.Create(mp.genPost)
	if err != nil {
		msg := "error creating html file"
		return &ProcessPostError{msg, err}
	}

	cfg := map[string]string{
		"pageTitle": mp.title,
		"title":     mp.title,
		"author":    mp.author,
		"date":      mp.date,
	}

	err = t.ExecuteTemplate(f, "main", cfg)
	if err != nil {
		msg := "error executing template file"
		return &ProcessPostError{msg, err}
	}

	err = f.Close()
	if err != nil {
		msg := "error closing file"
		return &ProcessPostError{msg, err}
	}

	return nil
}

func (mp *mdPost) saveToDB(db database.PostDB, dbPost database.Post, newPost bool) error {
	dbPost.FilePath = mp.filePath
	dbPost.FileName = mp.fileName
	dbPost.GenTemplate = &mp.genTemplate // Pointer, so that sqlx can parse potential db NULL value into Go nil value.
	dbPost.GenPost = mp.genPost
	dbPost.ModTime = mp.modTime
	dbPost.Title = mp.title
	dbPost.Author = mp.author

	if newPost {
		err := db.InsertPost(dbPost)
		if err != nil {
			return err
		}
	} else {
		err := db.UpdatePost(dbPost)
		if err != nil {
			return err
		}
	}

	return nil
}

func (tp *txtPost) parseContent() error {
	f, err := os.Open(tp.filePath)
	if err != nil {
		msg := "error opening file"
		return &ProcessPostError{msg, err}
	}

	lineNumber := 0
	s := bufio.NewScanner(f)
	s.Split(bufio.ScanLines)

	var body []string
	for s.Scan() {
		line := s.Text()

		if strings.HasPrefix(line, "Title: ") && lineNumber == 0 {
			tp.title = strings.TrimPrefix(line, "Title: ")
			lineNumber++
			continue
		} else if strings.HasPrefix(line, "Date: ") && lineNumber == 1 {
			tp.date = strings.TrimPrefix(line, "Date: ")
			lineNumber++
			continue
		} else if strings.HasPrefix(line, "Author: ") && lineNumber == 2 {
			tp.author = strings.TrimPrefix(line, "Author: ")
			lineNumber++
		} else if lineNumber > 2 {
			body = append(body, line)
		}
		lineNumber++
	}

	tp.body = strings.Join(body, "\n")

	err = f.Close()
	if err != nil {
		msg := "error closing file"
		return &ProcessPostError{msg, err}
	}

	return nil
}

func (tp *txtPost) genHtml() error {
	t, err := template.ParseFiles(tBlogpost, tMain, tNav)
	if err != nil {
		msg := "error parsing template files"
		return &ProcessPostError{msg, err}
	}

	fileName := fmt.Sprintf("%s.html", strings.TrimSuffix(tp.fileName, ".txt"))
	tp.genPost = fmt.Sprintf("%s/%s", genPostPath, fileName)

	f, err := os.Create(tp.genPost)
	if err != nil {
		msg := "error creating html file"
		return &ProcessPostError{msg, err}
	}

	cfg := map[string]string{
		"pageTitle": tp.title,
		"title":     tp.title,
		"author":    tp.author,
		"date":      tp.date,
		"body":      tp.body,
	}

	err = t.ExecuteTemplate(f, "main", cfg)
	if err != nil {
		msg := "error executing template file"
		return &ProcessPostError{msg, err}
	}

	err = f.Close()
	if err != nil {
		msg := "error closing file"
		return &ProcessPostError{msg, err}
	}

	return nil
}

func (tp *txtPost) saveToDB(db database.PostDB, dbPost database.Post, newPost bool) error {
	dbPost.FilePath = tp.filePath
	dbPost.FileName = tp.fileName
	dbPost.GenPost = tp.genPost
	dbPost.ModTime = tp.modTime
	dbPost.Title = tp.title
	dbPost.Author = tp.author

	if newPost {
		err := db.InsertPost(dbPost)
		if err != nil {
			return err
		}
	} else {
		err := db.UpdatePost(dbPost)
		if err != nil {
			return err
		}
	}
	return nil
}

func ParsePosts(dirPath string, db database.PostDB) error {
	rawFiles, err := os.ReadDir(dirPath)
	if err != nil {
		msg := fmt.Sprintf("error reading post files from directory: %s", dirPath)
		return &ProcessPostError{msg, err}
	}

	for _, f := range rawFiles {
		newPost := true

		fInfo, err := f.Info()
		if err != nil {
			msg := fmt.Sprintf("error getting file info for file: %s", f.Name())
			return &ProcessPostError{msg, err}
		}

		p := database.Post{FileName: f.Name()}
		posts, err := db.SelectPost(p)
		if err != nil {
			msg := fmt.Sprintf("error selecting post with name '%s' from db", f.Name())
			return &ProcessPostError{msg, err}
		}

		if len(posts) == 1 {
			newPost = false
			p = posts[0]

			dbModTime, err := time.Parse(time.RFC3339, p.ModTime)
			if err != nil {
				msg := "error parsing database modtime into time object"
				return &ProcessPostError{msg, err}
			}

			if dbModTime.Equal(fInfo.ModTime().Truncate(time.Second)) {
				continue // ModTime of local file == database entry, so file hasn't been modified.
			}
		} else if len(posts) > 1 {
			msg := fmt.Sprintf("got multiple posts with name '%s' from db, but expected one", f.Name())
			return &ProcessPostError{msg, err}
		}

		ext, err := util.GetFileExtension(f.Name())
		if err != nil {
			msg := fmt.Sprintf("error getting file extension of file '%s'", f.Name())
			return &ProcessPostError{msg, err}
		}

		switch ext {
		case ".txt":
			var tp = txtPost{
				fileName: f.Name(),
				filePath: fmt.Sprintf("%s%s", dirPath, f.Name()),
				modTime:  fInfo.ModTime().Format(time.RFC3339),
			}

			err = tp.parseContent()
			if err != nil {
				msg := fmt.Sprintf("error parsing txt post '%s'", tp.fileName)
				return &ProcessPostError{msg, err}
			}

			err = tp.genHtml()
			if err != nil {
				msg := fmt.Sprintf("error generating html for post '%s'", tp.fileName)
				return &ProcessPostError{msg, err}
			}

			err := tp.saveToDB(db, p, newPost)
			if err != nil {
				msg := "error saving text post to db"
				return &ProcessPostError{msg, err}
			}
		case ".md":
			var mp = mdPost{
				fileName: f.Name(),
				filePath: fmt.Sprintf("%s%s", dirPath, f.Name()),
				modTime:  fInfo.ModTime().Format(time.RFC3339),
			}

			err = mp.parseContent()
			if err != nil {
				msg := fmt.Sprintf("error parsing markdown post '%s'", mp.fileName)
				return &ProcessPostError{msg, err}
			}

			err = mp.genHtml()
			if err != nil {
				msg := fmt.Sprintf("error generating html for markdown post '%s'", mp.fileName)
				return &ProcessPostError{msg, err}
			}

			err := mp.saveToDB(db, p, newPost)
			if err != nil {
				msg := "error saving markdown post to db"
				return &ProcessPostError{msg, err}
			}
		default:
			return fmt.Errorf("unrecognized or unimplemented file type '%s'", ext)
		}
	}

	return nil
}

func CleanDeletedPosts(c config.Config, db database.PostDB) error {
	posts, err := db.SelectPost(database.Post{})
	if err != nil {
		return fmt.Errorf("error selecting post from db: %w", err)
	}

	files, err := os.ReadDir(c.Posts.Path)
	if err != nil {
		return fmt.Errorf("error reading directory '%s': %w", c.Posts.Path, err)
	}

	for _, p := range posts {
		found := false

		for _, f := range files {
			if p.FileName == f.Name() {
				found = true
				break
			}
		}

		if found {
			continue
		}

		err = os.Remove(p.GenPost)
		if err != nil && !os.IsNotExist(err) {
			return fmt.Errorf("error deleting generated post from disk: %w", err)
		}

		if p.GenTemplate != nil {
			err = os.Remove(*p.GenTemplate)
			if err != nil && !os.IsNotExist(err) {
				return fmt.Errorf("error deleting generated post from disk: %w", err)
			}
		}

		err = db.DeletePost(p)
		if err != nil {
			return fmt.Errorf("error deleting post from db: %w", err)
		}
	}

	return nil
}
