package errors

import "fmt"

// TODO - Fix error handling; Most errors end up here, but crashing for every error is stupid.
func HandleError(msg string, err error) {
	if err != nil {
		errString := fmt.Sprintf("%s: %s", msg, err)
		panic(errString)
	}
}
