package logging

import (
	"fmt"
	"io"
	"os"
	"time"
)

const (
	FATAL   = 0
	ERROR   = 1
	WARNING = 2
	INFO    = 3
	DEBUG   = 4
)

// Logger Format can contain any of the supported keywords:
// "timestamp" 	: Simple RFC3339 timestamp
// "host" 		: Hostname of webserver
// "level"		: Loglevel
// "msg"		: Log message
type Logger struct {
	Format []string
	Level  int
	Outs   []io.Writer
}

// New creates a Logger with sane defaults for generating regular text logs.
func New() *Logger {
	return &Logger{
		Format: []string{"timestamp", "host", "level", "msg"},
		Level:  INFO,
		Outs:   []io.Writer{os.Stdout},
	}
}

func (t Logger) Print(level int, msg string) error {
	if level < t.Level {
		return nil
	}

	msg, err := applyFormat(t.Format, level, msg)
	if err != nil {
		return fmt.Errorf("error applying log format %w", err)
	}

	for _, out := range t.Outs {
		_, err := fmt.Fprint(out, msg)
		if err != nil {
			return fmt.Errorf("error printing log message to output")
		}
	}

	return nil
}

func (t Logger) Printf(level int, format, msg string) error {
	if level < t.Level {
		return nil
	}

	msg = fmt.Sprintf(format, msg)
	msg, err := applyFormat(t.Format, level, msg)
	if err != nil {
		return fmt.Errorf("error applying log format %w", err)
	}

	for _, out := range t.Outs {
		_, err := fmt.Fprint(out, msg)
		if err != nil {
			return fmt.Errorf("error printing log message to output")
		}
	}

	return nil
}

func (t Logger) Println(level int, msg string) error {
	if level < t.Level {
		return nil
	}

	msg, err := applyFormat(t.Format, level, msg)
	if err != nil {
		return fmt.Errorf("error applying log format %w", err)
	}

	for _, out := range t.Outs {
		_, err := fmt.Fprintln(out, msg)
		if err != nil {
			return fmt.Errorf("error printing log message to output")
		}
	}

	return nil
}

func applyFormat(format []string, level int, msg string) (string, error) {
	var formatted string

	for _, key := range format {
		switch {
		case key == "timestamp":
			timestamp := time.Now().Format(time.RFC3339)
			formatted = fmt.Sprintf("%s [%s]", formatted, timestamp)
		case key == "host":
			host, err := os.Hostname()
			if err != nil {
				return formatted, fmt.Errorf("error getting hostname")
			}
			formatted = fmt.Sprintf("%s %s", formatted, host)
		case key == "level":
			formatted = fmt.Sprintf("%s %d", formatted, level)
		case key == "msg":
			formatted = fmt.Sprintf("%s %s", formatted, msg)
		default:
			return formatted, fmt.Errorf("invalid format key: '%s'", key)
		}
	}

	return formatted, nil
}
