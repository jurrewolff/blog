package route

import (
	"blog/pkg/database"
	"blog/pkg/logging"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"strings"
)

func NotFoundHandler(w http.ResponseWriter, r *http.Request, logger *logging.Logger) {
	logger.Printf(logging.INFO, "received request with invalid path: %s\n", r.URL.Path)

	w.WriteHeader(http.StatusNotFound)
	_, err := fmt.Fprintf(w, "404 not found")
	if err != nil {
		fmt.Printf("error writing not found response: %s\n", err)
	}
}

func ServeFavicon(logger *logging.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/favicon.ico" {
			NotFoundHandler(w, r, logger)
			return
		}

		w.WriteHeader(http.StatusNoContent)
	}
}

func ServeRoot(db database.PostDB, logger *logging.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			NotFoundHandler(w, r, logger)
			return
		}

		tMain := filepath.Join("web", "template", "main.html")
		tHomeContent := filepath.Join("web", "template", "homecontent.html")
		tNav := filepath.Join("web", "template", "gen_templ", "navigation.html")
		t, _ := template.ParseFiles(tMain, tHomeContent, tNav)

		type post struct {
			Title string
			Url   string
		}

		var homepagePosts []post

		dbPosts, err := db.SelectPost(database.Post{})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Fatalf("error selecting posts from db: %s", err)
		}

		for _, p := range dbPosts {
			url := strings.TrimPrefix(p.GenPost, "./web")
			homepagePosts = append(homepagePosts, post{Title: p.Title, Url: url})
		}

		data := map[string]interface{}{
			"pageTitle": "Home",
			"posts":     homepagePosts,
			"title":     "Posts",
		}

		err = t.ExecuteTemplate(w, "main", data)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Fatalf("error executing template: %s", err)
		}
	}
}

func ServeAbout(db database.PostDB, logger *logging.Logger) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		tMain := filepath.Join("web", "template", "main.html")
		tAboutContent := filepath.Join("web", "template", "aboutcontent.html")
		tNav := filepath.Join("web", "template", "gen_templ", "navigation.html")
		t, _ := template.ParseFiles(tMain, tAboutContent, tNav)

		data := map[string]interface{}{
			"pageTitle": "About",
			"title":     "About",
		}

		err := t.ExecuteTemplate(w, "main", data)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Fatalf("error executing template: %s", err)
		}
	}
}
