package route

import (
	"blog/pkg/database"
	"blog/pkg/logging"
	"net/http"
)

func SetRoutes(db database.PostDB, logger *logging.Logger) *http.ServeMux {
	mux := http.DefaultServeMux

	// Static
	mux.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("./web/static/"))))

	// Semi-statics
	mux.HandleFunc("/", ServeRoot(db, logger))
	mux.HandleFunc("/about", ServeAbout(db, logger))
	mux.HandleFunc("/favicon.ico", ServeFavicon(logger))

	return http.DefaultServeMux
}
